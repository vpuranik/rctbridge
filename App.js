/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {NativeModules, NativeEventEmitter} from 'react-native';

console.log(NativeModules.CalendarManager)
const calendarManagerEmitter = new NativeEventEmitter(NativeModules.CalendarManager)
const subscription = calendarManagerEmitter.addListener(
  'EventReminder',
  (reminder) => {
    console.log('Event')
    console.log('Name:' + reminder.name)
    console.log('Location:' + reminder.location)
    console.log('Date:' + reminder.date)
  }
);

NativeModules.CalendarManager.addEvent('One', 'Two', 3, function(o) {
  console.log('In Callback')
  console.dir(o)
});

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
